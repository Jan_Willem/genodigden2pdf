# Copyright (C) 2023 janwillem
# 
# This file is part of genodigden2pdf.
# 
# genodigden2pdf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# genodigden2pdf is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with genodigden2pdf.  If not, see <http://www.gnu.org/licenses/>.

"""
genodigden2pdf - Maak de labels voor de genodigden gebaseerd op een CSV bestand.

Usage: genodigden2pdf INPUT.CSV

"""

import os
import sys
from docopt import docopt
from md2pdf.core import md2pdf
import csv as csvLoader
import math
from jinja2 import Environment, FileSystemLoader, select_autoescape
from importlib_resources import files

def main(argv=None):
    # Parse command line arguments
    arguments = docopt(__doc__, version="genodigden2pdf %s" % '0.1')

    # Paths
    csv_file_path = arguments.get("INPUT.CSV")
    base_url = os.getcwd()

    csv = get_csv(csv_file_path)
    html = get_html(csv)

    md2pdf(
        'genodigden.pdf',
        md_content=html,
        css_file_path=files('template').joinpath('template.css'),
        base_url=base_url,
    )


def get_csv(path: str) -> list:
    if (not os.path.isfile(path)):
        raise FileNotFoundError(path + " not found")
    with open(path, newline="") as csvfile:
        return list(csvLoader.DictReader(csvfile, delimiter=","))


def get_html(csv: list) -> str:
    env = Environment(
        loader=FileSystemLoader(searchpath=files('template').joinpath('template.html.jinja')), autoescape=select_autoescape()
    )
    template = env.get_template("")

    # weasyprint has some bugs regaring div splitting on pages
    # we generate the html in chunck that fit exactly on one page (8 x 3)
    size = 8 * 3
    html = ""
    for i in range(math.ceil(len(csv) / size)):
        begin = i * size
        sub = csv[begin : size + begin]
        html = html + template.render(items=sub) +  "\n"
    return html


if __name__ == "__main__":
    sys.exit(main())
