# genodigden2pdf

Maak op basis van een `.csv` bestand een `.pdf` bestand met de addressen correct geformateerd.

### Benodigde software:
- Python (windows): <https://www.python.org/downloads/windows/>
- De code voor deze tool: <https://gitlab.com/Jan_Willem/genodigden2pdf>

### Installeren

Open een terminal in de folder: `genodigden2pdf`

Installeer de tool door het volgdende command:
```sh
pip install .
```

### Gebruiken
Deze tool heeft een `.csv` als input nodig. Er worden de volgdende colomen gebruikt:
- `Naam organisatie`
- `Adres`
- `Postcode`
- `Plaats`

Om de tool te runnen open een terminal in de folder: `genodigden2pdf` en voor het volgende command uit:

```sh
python genodigden2pdf < locatie csv bestand >
```

Vervolgens verschijnt er een `genodigden.pdf`.

